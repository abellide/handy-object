const objectContainsChilds = (obj, ...childs) => {
    if (!!obj && !!childs) {
        let exists = true;
        let item = obj;
        childs.forEach((key) => {
            if (!!item[key]) {
                item = item[key];
            } else {
                exists = false;
            }
        });
        return exists;
    }
    return false;
};

const getValueCheckingParentsIntegrity = (obj, ...childs) => {
    if (!!obj && !!childs) {
        let exists = true;
        let item = obj;
        let r = null
        childs.forEach((key, i) => {
            if (!!item[key] && i < childs.length - 1) {
                item = item[key];
            } else if (!!item[key] && i === childs.length - 1) {
                r = item[key];
            } else {
                exists = false;
            }
        });
        return r;
    }
    return null;
};

const getValueOrDefaultCheckingParentsIntegrity = (obj, def, ...childs) => {
    if (!!obj && !!childs) {
        let exists = true;
        let item = obj;
        let r = def
        childs.forEach((key, i) => {
            if (!!item[key] && i < childs.length - 1) {
                item = item[key];
            } else if (!!item[key] && i === childs.length - 1) {
                r = item[key];
            } else {
                exists = false;
            }
        });
        return r;
    }
    return def;
};

module.exports = {
    objectContainsChilds,
    getValueCheckingParentsIntegrity,
    getValueOrDefaultCheckingParentsIntegrity,
};